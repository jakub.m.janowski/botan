## Botan proxy repository for hell

This is proxy repository for [botan library](https://github.com/randombit/botan), which allow you to build and install it using [hell dependency manager](https://gitlab.com/rilis/hell/hell).

* If you have problem with installation of botan using hell, have improvement idea, or want to request support for other versions of botan, then please [create issue here](https://gitlab.com/rilis/rilis/issues).
* If you found bug in botan itself please create issue at [botan issue tracker](https://github.com/randombit/botan/issues), because here we don't do any kind of botan development.

## Important Notes

This package except hell `required targets` additionally supports `inject targets`:
* `BOTAN_CPU_TYPE` - set the target CPU type/model
* `BOTAN_OS_TYPE` - set the target operating system
* `BOTAN_CC_TYPE` - set the desired build compiler
* `BOTAN_CC_PATH` - set path to compiler binary
* `BOTAN_AR_PATH` - set path to static archive creator
* `BOTAN_ENDIAN` - override byte order guess
* `BOTAN_ABI_FLAGS` - set compiler ABI flags
* `BOTAN_CXX_FLAGS` - set compiler flags
* `BOTAN_LINKER_FLAGS` - set linker flags
* `BOTAN_EXTERNAL_LIBDIR_PATH` - use DIR for external libs
* `BOTAN_EXTERNAL_INCLUDE_DIR_PATH` - use DIR for external includes
* `BOTAN_CUSTOM_MAKE_TEMPLATE_PATH` -if defined, before executing botan `configure.py` provided file will be used to replace botan `makefile tamplate`. 

These options are `last hope` for some odd platfroms, where cmake autodetection or crosscompilation toolchain configuration is not good enough. Please use these options very carefully and when you really know what you are doing. 
Typically you should not need to use them.
